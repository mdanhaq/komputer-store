const loanElement=document.getElementById("loan"); //loan button
const bankElement=document.getElementById("bank"); // bank button
const workElement=document.getElementById("work"); // work button
const laptopsElement = document.getElementById("laptops"); // laptops select list
const featureElement = document.getElementById("feature"); // feature of the selected laptop shown just next
const descriptionElement = document.getElementById("description"); // description of the selected laptop on the bottom part
const payElement=document.getElementById("pay"); // amount of money earned after work
const balanceElement= document.getElementById("balance"); // amount of balance
const pictureElement = document.getElementById("picture");
const showPriceElement = document.getElementById("showPrice");
const buyElement = document.getElementById("buy");
const outstandingElement = document.getElementById("outstanding");
const outstandingLoanButtonElement = document.getElementById("outstandingLoan");
const specsElement=document.getElementById("specs");

let laptops = [];
let balance = 200;
let pay = 0;
let count = 0;
let selectToBuyLaptopPrice = 0;
let interestedAmountOfLoan = 0;
let link = "https://noroff-komputer-store-api.herokuapp.com/";

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))

const addLaptopsToMenu = (laptops) =>{
    laptops.forEach(x => addLaptopToMenu(x));
    laptopElement.innerText = laptops[0].title;
    featureElement.innerText = 'It has  ' + laptops[0].specs[0].toLowerCase() + '\n It has ' +  laptops[0].specs[1].toLowerCase();   
    const defaultImage=link + laptops[0].image;
    pictureElement.innerHTML = `<img src = "${defaultImage}" width= "200" height= "110"/>`;
    showPriceElement.innerText = laptops[0].price + ' Kr.';
    descriptionElement.innerText = laptops[0].title +'\n \n'+laptops[0].description;
}

const addLaptopToMenu = (laptop) =>{
    laptopElement =document.createElement("option");
    laptopElement.value=laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

const handleLaptopMenuChange = e => {
    selectedLaptop = laptops[e.target.selectedIndex];
    featureElement.innerText = 'It has  ' + selectedLaptop.specs[0].toLowerCase() + '\n It has ' +  selectedLaptop.specs[1].toLowerCase();   
    const selectedLaptopSpecsLength = selectedLaptop.specs.length;
    const getLaptopImage = link + selectedLaptop.image;
    pictureElement.innerHTML = `<img src = "${getLaptopImage}" width= "200" height= "110"/>`;
    descriptionElement.innerText = selectedLaptop.title +'\n \n'+selectedLaptop.description +'\n'+ 
    selectedLaptop.specs[selectedLaptopSpecsLength-2]+ '\n'+selectedLaptop.specs[selectedLaptopSpecsLength-1];
    showPriceElement.innerText = `${selectedLaptop.price} Kr. `;
    selectToBuyLaptopPrice = selectedLaptop.price;
}

const handleWork = () => {
    pay += 100;
    payElement.innerText = `${pay.toFixed(2)} kr`;
}

const handleBank = () => {
    if(count === 0){
        balance +=  pay;
        balanceElement.innerText =`${balance.toFixed(2)} kr`;
        pay = 0;
        payElement.innerText = `${pay.toFixed(2)} kr`;  
    }
    else if(count === 1)
    {
            balance += pay * 0.9;
            interestedAmountOfLoan -= pay * 0.1;
            pay = 0;
            payElement.innerText =`${pay.toFixed(2)} kr`;
            balanceElement.innerText =`${balance.toFixed(2)} kr`;
            outstandingElement.innerText = `Outstanding loan: ${interestedAmountOfLoan.toFixed(2)} kr`;
            
            if(interestedAmountOfLoan === 0){
            count = 0;
            outstandingLoanButtonElement.style.visibility = "hidden";
            outstandingElement.style.visibility = "hidden";
            }   
    }
}

const handleLoan = () => {
    while (count === 0)
    {
        interestedAmountOfLoan = parseInt(prompt("How much money you want to loan: "));
        
        if(Number.isNaN(interestedAmountOfLoan)) 
            alert("NB.: You have not entered a number");
                else if (interestedAmountOfLoan > balance *2)
                    alert("NB.: You are not eligible for that amount");
                        else
                            {
                                alert("Congratulations!!! You are eligible for that amount");
                                balance += interestedAmountOfLoan;
                                balanceElement.innerText =`${balance.toFixed(2)} kr`;
                                outstandingElement.style.visibility = "visible";
                                outstandingElement.innerText = `Outstanding loan: ${interestedAmountOfLoan.toFixed(2)} kr`;
                                outstandingLoanButtonElement.style.visibility = "visible";
                                count++;
                            }
    }    
}

const handleRemainingLoan = () => {
        if(interestedAmountOfLoan > pay){
            interestedAmountOfLoan = interestedAmountOfLoan - pay;
            pay = 0;
            payElement.innerText = `${pay} kr`;
            outstandingElement.innerText = `Outstanding loan: ${interestedAmountOfLoan} kr`;
        }
        if(interestedAmountOfLoan <= pay){
            pay = pay - interestedAmountOfLoan;
            outstandingElement.style.visibility = "hidden";
            outstandingLoanButtonElement.style.visibility = "hidden";
            payElement.innerText = `${pay} kr`;
            count = 0;
        }
    } 

const handleBuy = () => {
    if(balance >= selectToBuyLaptopPrice){
        balance = balance - selectToBuyLaptopPrice;
        balanceElement.innerText =`${balance.toFixed(2)} kr`;
        alert("Congratulation!!! \n You are now owner of the new laptop");
    }
    else
        alert("Unfortunately, you cannot afford the laptop");
}

laptopsElement.addEventListener("change", handleLaptopMenuChange);
workElement.addEventListener("click", handleWork);
bankElement.addEventListener("click", handleBank);
loanElement.addEventListener("click", handleLoan);
buyElement.addEventListener("click", handleBuy);
outstandingLoanButtonElement.addEventListener("click", handleRemainingLoan);